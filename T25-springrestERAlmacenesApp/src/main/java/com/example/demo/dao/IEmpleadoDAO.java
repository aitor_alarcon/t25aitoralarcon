package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.empleado;

public interface IEmpleadoDAO extends JpaRepository<empleado, String>{

}
