package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "empleados")
public class empleado {
	
	@Id
	private String id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "apellidos")
	private String apellidos;
	
	@ManyToOne
	@JoinColumn(name = "departamento")
	private departamento departamento;

	//Constructores
	
	public empleado() {
		
	}

	
	/**
	 * @param id
	 * @param nombre
	 * @param apellidos
	 * @param departamento
	 */
	
	public empleado(String id, String nombre, String apellidos, com.example.demo.dto.departamento departamento) {
		//super();
		this.id = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.departamento = departamento;
	}


	//Getters y Setter
	
	/**
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * 
	 * @return the apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * 
	 * @param apellidos the apellidos to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * 
	 * @return the departamento
	 */
	public departamento getDepartamento() {
		return departamento;
	}

	/**
	 * 
	 * @param departamento the departamento to set
	 */
	public void setDepartamento(departamento departamento) {
		this.departamento = departamento;
	}

	//toString
	@Override
	public String toString() {
		return "empleado [id=" + id + ", nombre=" + nombre + ", apellidos=" + apellidos + ", departamento="
				+ departamento + "]";
	}
	
	
}
