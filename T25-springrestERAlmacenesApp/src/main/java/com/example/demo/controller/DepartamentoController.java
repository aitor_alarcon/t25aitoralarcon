package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.departamento;
import com.example.demo.service.departamentoServiceImpl;

@RestController
@RequestMapping("/api")
public class DepartamentoController {

	@Autowired
	departamentoServiceImpl departamentoServiceImpl;
	
	@GetMapping("/departamentos")
	public List<departamento> listarDepartamentos(){
		return departamentoServiceImpl.listarDepartamentos();
	}
	
	@PostMapping("/departamentos")
	public departamento guardarDepartamento(@RequestBody departamento departamento) {
		return departamentoServiceImpl.guardarDepartamento(departamento);
	}
	
	@GetMapping("/departamentos/{id}")
	public departamento departamentoID(@PathVariable(name = "id") Integer id) {
		
		departamento departamento_ID = new departamento();
		
		departamento_ID = departamentoServiceImpl.departamentoID(id);
		
		System.out.println("Departamento ID: " + departamento_ID);
		
		return departamento_ID;
	}
	
	@PutMapping("/departamentos/{id}")
	public departamento actualizarDepartamento(@PathVariable(name = "id") Integer id, @RequestBody departamento departamento) {
		
		departamento departamento_sel = new departamento();
		departamento departamento_act = new departamento();
		
		departamento_sel = departamentoServiceImpl.departamentoID(id);
		
		departamento_sel.setNombre(departamento.getNombre());
		departamento_sel.setPresupuesto(departamento.getPresupuesto());
		
		departamento_act = departamentoServiceImpl.actualizarDepartamento(departamento_sel);
		
		System.out.println("El empleado actualizado es: " + departamento_act);
		
		return departamento_act;
		
	}
	
	@DeleteMapping("/departamentos/{id}")
	public void eliminarDepartamento(@PathVariable(name = "id") Integer id) {
		departamentoServiceImpl.eliminarDepartamento(id);
	}
}
