DROP table IF EXISTS empleados CASCADE;
DROP table IF EXISTS departamentos CASCADE;

CREATE TABLE `departamentos`(
`id` integer auto_increment NOT NULL,
`nombre` nvarchar(100),
`presupuesto` integer,
PRIMARY KEY (`id`)
);     

CREATE TABLE `empleados`(
`id` varchar(8) NOT NULL,
`nombre` nvarchar(100),
`apellidos` nvarchar(250),
`departamento` integer,
PRIMARY KEY (`id`),
CONSTRAINT `empleados_fk` FOREIGN KEY (`departamento`) REFERENCES `departamentos` (`id`) 
);                              

insert into departamentos (id, nombre, presupuesto)values(1,'IT', 1500);
insert into departamentos (id, nombre, presupuesto)values(2,'Dirección', 1500);
insert into departamentos (id, nombre, presupuesto)values(3,'Contabilidad', 2000);

insert into empleados (id, nombre, apellidos, departamento)values('39941483','Aitor','Alarcón', 2);
insert into empleados (id, nombre, apellidos, departamento)values('39956538','Alberto','García', 1);
insert into empleados (id, nombre, apellidos, departamento)values('39978632','Oscar','Trillas', 3);